<?php

session_start();

if(count($_SESSION) > 0){
    
echo "<h2>Información de la conexión</h2>";
echo "<hr>";
echo "<p>Rut Franquiciado: <b>{$_SESSION['rut_franquiciado']}</b></p>";
echo "<p>Usuario: <b>{$_SESSION['username']}</b></p>";
echo "<p>Password: <b>{$_SESSION['password']}</b></p>";
echo "<p>Conectado a la base de datos: <b>{$_SESSION['bd_franquiciado']}</b></p>";

// echo "se conectó a la base de datos  ".DB;
echo "<a href='inicio.php'>Ir a INICIO.PHP</a>";
echo "<a href='cerrarsesion.php'>Cerrar Sesión</a>";

}else{

    header('location: login.html');
}
?>